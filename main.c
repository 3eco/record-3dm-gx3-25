//
// 19.11.2015
// beco
//

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

unsigned char lpBuffer2[1500];


int main() {
    FILE* pLogFile;
    pLogFile = fopen("imu.bin","w");
    int fd = open("/dev/ttyACM0", O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
    write(fd, "\xC4\xC1\x29\xCB", 4);
    int counter = 10000000;
    while(counter > 0)
    {
        int rd = read(fd,&lpBuffer2,1000);
        if(rd != -1)
        {
            int i = 0;
            while(i<rd)
            {
                fprintf(pLogFile,"%c",lpBuffer2[i]);
                printf("%c", lpBuffer2[i]);
                i++;
            }
        }
        counter--;
    }
    fclose(pLogFile);
    return 0;
}
