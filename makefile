CFLAGS  =  -g

main.o:main.c
	$(COMPILE.c) $< -o $@

imu:main.o
	$(LINK.c) main.o -o $@
	@echo Type ./$@ to execute the program.

clean:
	$(RM) main.o imu
